import pandas as pd
import shutil
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import os


def create_directory_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def export_excel(selected_base, excel_name_list):
    source_excel = selected_base.get()

    try:
        excel_name_df = pd.read_excel(excel_name_list.get())
        # Mengambil semua elemen data dari kolom pertama
        elemen_data_list = excel_name_df.iloc[:, 0].tolist()

    except Exception as e:
        result_label.config(text=f"Terjadi kesalahan: {e}")
        return

    try:
        for elemen_data in elemen_data_list:
            # Buat elemen data baru
            destination_excel = "{}.xlsx".format(elemen_data)

            # Membuat direktori 'file_baru' jika belum ada
            create_directory_if_not_exists('file_baru')

            # Menyalin file Excel
            copy_excel_file(source_excel, os.path.join(
                'file_baru', destination_excel), elemen_data)
            result_label.config(text="SUKSES !")

    except Exception as e:
        result_label.config(text=f"Terjadi kesalahan: {e}")


def copy_excel_file(source_file, destination_file, excel_name):
    # Membaca file Excel sumber menggunakan pandas
    df = pd.read_excel(source_file)
    destination_file = destination_file.format(excel_name)
    # Menulis data ke file Excel tujuan
    df.to_excel(destination_file, index=False)


def browse_file_for_base_excel():
    filename = filedialog.askopenfilename(
        filetypes=[("Excel files", "*.xlsx")])
    selected_base.set(filename)
    selected_base_label.config(text=f"Selected Base Excel File: {filename}")


def browse_file_for_new_excel():
    filename = filedialog.askopenfilename(
        filetypes=[("Excel files", "*.xlsx")])
    selected_new_name.set(filename)
    selected_new_label.config(text=f"Selected New Excel File: {filename}")


# ************************************************************************************************ TKinter GUI

# Tkinter GUI
root = tk.Tk()
root.title("Excel Exporter")

# Label and Browse button for selecting the base Excel file
selected_base = tk.StringVar()

base_label = ttk.Label(root, text="Select Base Excel File:")
base_label.grid(row=0, column=0, padx=10, pady=10)

browse_button = ttk.Button(
    root, text="Browse", command=browse_file_for_base_excel)
browse_button.grid(row=0, column=1, padx=10, pady=10)

selected_base_label = ttk.Label(root, text="Selected Base Excel File:")
selected_base_label.grid(row=1, column=0, padx=10, pady=10)


# New excel file
selected_new_name = tk.StringVar()

new_name_excel_file = ttk.Label(root, text="Output Filename:")
new_name_excel_file.grid(row=2, column=0, padx=10, pady=10)

browse_button = ttk.Button(
    root, text="Browse", command=browse_file_for_new_excel)
browse_button.grid(row=2, column=1, padx=10, pady=10)

selected_new_label = ttk.Label(root, text="Selected New Excel File:")
selected_new_label.grid(row=3, column=0, padx=10, pady=10)


# Export button
export_button = ttk.Button(root, text="Export", command=lambda: export_excel(
    selected_base, selected_new_name))
export_button.grid(row=4, column=0, columnspan=2, pady=10)

# Result label
result_label = ttk.Label(root, text="")
result_label.grid(row=5, column=0, columnspan=2, pady=10)

root.mainloop()
